const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const { resolve, join } = require("path");

const cleanOptions = {
  root: __dirname,
  verbose: true,
  dry: false,
  exclude: [],
};

module.exports = {
  entry: [
    "react-hot-loader/patch",
    //activate HMR for React

    "webpack-dev-server/client?http://127.0.0.1:8080",
    //bundle the client for webpack dev server
    //and connect to the provided endpoint

    "webpack/hot/only-dev-server",
    //bundle the client for hot reloading
    //only- means to only hot reload for successful updates

    
		"./src/dev.js"
	],

  mode: "development",
  output: {
    path: resolve(__dirname, "./dist"),
    publicPath: "/",
    filename: "[name].js",
  },
  devServer: {
    static: {
      directory: join(__dirname, 'public'),
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    hot: true,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        exclude: /node_modules/,
        loader: "babel-loader",
      },
      {
				test: /\.(ttf)$/i,
				loader: "file-loader",
				options: {
	  		name: '[name].[ext]',
			}
      },
      {
        test: /\.(scss|css)$/,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(jpe?g|png|gif|svg|webp|jp2|jxr)$/i,
        loader: "file-loader",
				options: {
	  			name: '[name].[ext]',
				}
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(cleanOptions),
    new CopyPlugin({
      patterns: [
        {
          from: "public/*",
          to({ context, absoluteFilename }) {
            return "[name][ext]";
          },
        },
      ],
    }),
   new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
  ],
  resolve: {
    alias: {
      "react/lib/ReactDom": "react-dom/lib/ReactDom",
    },
    extensions: [".js", ".jsx"],
  },
};

