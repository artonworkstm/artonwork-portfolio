import React, { Component } from "react";
import Home from "./sceens/Home/Home";
import { isMobile } from "react-device-detect";
import { CookiesProvider } from "react-cookie";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobile: isMobile,
      read: false,
      top: false,
    };
  }

  setMobile = (e) => this.setState({ mobile: e });
  setRead = (e) => this.setState({ read: e });
  setTop = (e) => this.setState({ top: e });
  setCookie = (e) => this.setState({ cookie: e });

  render() {
    const { mobile, read, top } = this.state;
    return (
      <CookiesProvider>
        <Home
          mobile={mobile}
          setMobile={this.setMobile}
          read={read}
          setRead={this.setRead}
          top={top}
          setTop={this.setTop}
          setCookie={this.setCookie}
        />
      </CookiesProvider>
    );
  }
}

export default App;
