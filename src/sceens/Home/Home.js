//import liraries
import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Cookies } from "react-cookie";
import "./Home.scss";

import Main from "../../components/Main/Main";
import About from "../../components/About/About";
import Projects from "../../components/Projects/Projects";
import Admin from "../../components/Admin/Admin";
import Contact from "../../components/Contact/Contact";
import Headline from "../../components/Headline/Headline";
import Footer from "../../components/Footer/Footer";
import Hamburger from "../../components/Hamburger/Hamburger";
import Policy from "../../components/Policy/Policy";

// create a component
const Home = (props) => {
  let pageY = window.outerWidth,
    pageX = window.outerHeight;
  const { mobile, read, setRead, top, setTop, setCookie } = props;
  const [view, setView] = useState(pageY / pageX >= 1.7);

  let cookies = new Cookies();

  useEffect(() => {
    !mobile &&
      window.addEventListener("resize", (e) => {
        e.stopImmediatePropagation();

        pageY = window.outerWidth;
        pageX = window.outerHeight;
        const ratio = pageY / pageX >= 1.7;

        setView(ratio);
      });

    !mobile &&
      window.addEventListener("scroll", (e) => {
        e.stopImmediatePropagation();
        const currentY = window.scrollY;

        if (currentY > pageY) {
          setRead(true);
        } else {
          setRead(false);
        }

        currentY >= 200 ? setTop(true) : setTop(false);

        pageY = currentY;
      });
  });

  return (
    <Router>
      <main className="bgcContainer" />
      <Hamburger mobile={mobile} top={top} />
      <Headline top={top} read={read} view={mobile} />
      <Switch>
        <Route path="/" exact render={() => <Main view={mobile} />} />
        <Route path="/about" exact component={About} />
        <Route path="/projects" exact component={Projects} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/admin" exact component={Admin} />
      </Switch>
      {!mobile && view && <Footer read={top} />}
      {!cookies.get("artonwork") && <Policy setCookie={setCookie} />}
    </Router>
  );
};

//make this component available to the app
export default Home;
