//import liraries
import { Canvas } from "@react-three/fiber";
import React, { Suspense } from "react";
import { StopSwing } from "./StopSwing";

// create a component named PageSwing
export const PageSwing = () => {
  return (
    <Canvas
      className="gltf-avatar"
      style={{ height: "100vh", width: "100vw" }}
      camera={{ position: [0, 0, 2] }}
      color={{ attach: "background", args: ["transparent"] }}
    >
      <directionalLight color="blue" position={[0, 0, 5]} />
      <pointLight color="purple" position={[10, 10, 10]} />
      <ambientLight intensity={0.1} />
      <Suspense fallback={"<h1>Loading avatar</h1>"}>
        <StopSwing
          scale={[2, 2, 2]}
          position={[-3, -4.5, -7]}
          rotation={[0.2, 0.4, 0]}
        />
      </Suspense>
    </Canvas>
  );
};
