//import liraries
import { Canvas } from "@react-three/fiber";
import React, { Suspense } from "react";
import { IdleWaving } from "./IdleWaving";

// create a component named Waving
const Waving = () => {
  return (
    <Canvas
      className="portre"
      style={{ height: "20rem", width: "20rem" }}
      camera={{ position: [-0.1, 0.6, 1.2], rotation: [-0.2, 0, 0] }}
      color={{ attach: "background", args: ["transparent"] }}
    >
      <directionalLight color="blue" position={[0, 0, 5]} />
      <pointLight color="purple" position={[10, 10, 10]} />
      <ambientLight intensity={0.1} />
      <Suspense fallback={"<h1>Loading avatar</h1>"}>
        <IdleWaving
          scale={[1, 1, 1]}
          position={[0, -1, 0.3]}
          rotation={[0, 0, 0]}
        />
      </Suspense>
    </Canvas>
  );
};

//make this component available to the app
export default Waving;
