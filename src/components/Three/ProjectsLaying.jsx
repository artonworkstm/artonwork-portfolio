//import liraries
import { Canvas } from "@react-three/fiber";
import React, { Suspense } from "react";
import { Laying } from "./Laying";

// create a component named ProjectsLaying
const ProjectsLaying = () => {
  return (
    <Canvas
      className="gltf-avatar"
      style={{ height: "100vh", width: "100vw" }}
      camera={{ position: [0, 0, 0], rotation: [0, 0, 0] }}
      color={{ attach: "background", args: ["transparent"] }}
    >
      <directionalLight color="blue" position={[0, 0, 5]} />
      <pointLight color="purple" position={[10, 10, 10]} />
      <ambientLight intensity={0.1} />
      <Suspense fallback={"<h1>Loading...</h1>"}>
        <Laying position={[0, -1.5, -2.8]} rotation={[-0.4, 0, 0]} />
      </Suspense>
    </Canvas>
  );
};

//make this component available to the app
export default ProjectsLaying;
