import React from "react";
import { Canvas } from "@react-three/fiber";

import "./Animations.scss";
import Timeline from "./Timeline";

const Animations = () => {
  return (
    <Canvas
      className="gltf-avatar"
      style={{ height: "100vh", width: "100vw" }}
      camera={{ position: [0, 0, 2] }}
      color={{ attach: "background", args: ["transparent"] }}
    >
      <directionalLight color="blue" position={[0, 0, 5]} />
      <pointLight color="purple" position={[10, 10, 10]} />
      <ambientLight intensity={0.1} />

      <Timeline />
    </Canvas>
  );
};

export default Animations;
