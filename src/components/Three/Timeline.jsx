//import liraries
import { useFrame, useThree } from "@react-three/fiber";
import React, { Suspense, useState } from "react";
import { Crouch } from "./Crouch";

// create a component named Timeline
const Timeline = () => {
  const [time, setTime] = useState(0);
  const clock = useThree().clock;

  useFrame(() => {
    setTime(clock.getElapsedTime());
  });

  return (
    <Suspense fallback={"<h1>Loading avatar</h1>"}>
      <Crouch
        scale={[1.7, 1.7, 1.7]}
        position={[5, -4, -4]}
        paused={time < 2.7 ? false : true}
      />
    </Suspense>
  );
};

//make this component available to the app
export default Timeline;
