//import liraries
import React, { useState } from "react";
import { Switch, Route, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import "./Hamburger.scss";

// create a component
const Hamburger = (props) => {
  const { top } = props;
  const [sandwich, setSandwich] = useState(false);

  const handleExit = (e) => {
    if (e.target === e.currentTarget) {
      setSandwich(!sandwich);
    }
  };

  return (
    <section className="hamburger">
      <section
        className={`headline${sandwich ? " headline-active" : ""}`}
        style={{
          boxShadow: top
            ? "inset 0px 2px 15px #2b0261, 0px 2px 15px #2b0261"
            : "inset 0px 2px 15px #2b0261",
        }}
      >
        <button onClick={() => setSandwich(!sandwich)} aria-label="menu">
          <FontAwesomeIcon icon={faBars} />
        </button>
        <section className="title">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" component={About} />
            <Route path="/projects" component={Projects} />
            <Route path="/contact" component={Contact} />
          </Switch>
        </section>
      </section>
      <section
        className="exit"
        onClick={(e) => handleExit(e)}
        style={{
          display: sandwich ? "block" : "none",
        }}
      >
        <section className="menu">
          <Link to="/" onClick={() => setSandwich(false)} aria-label="home">
            <h4>home</h4>
          </Link>
          <Link
            to="/projects"
            onClick={() => setSandwich(false)}
            aria-label="projects"
          >
            <h4>projects</h4>
          </Link>
          <Link
            to={{ pathname: "https://blog.artonwork.com" }}
            target="_blank"
            onClick={() => setSandwich(false)}
            aria-label="contact"
          >
            <h4>blog</h4>
          </Link>
          <section className="footer">
            <a
              href="https://github.com/Puding07"
              target="_blank"
              rel="noopener noreferrer"
              aria-label="github"
            >
              <FontAwesomeIcon
                icon={["fab", "github"]}
                style={{ fontSize: 26 }}
              />
            </a>

            <a
              href="https://www.linkedin.com/in/%C3%A1ron-t%C3%B3th-5825841a8/"
              target="_blank"
              rel="noopener noreferrer"
              aria-label="linkedin"
            >
              <FontAwesomeIcon
                icon={["fab", "linkedin-in"]}
                style={{ fontSize: 26 }}
              />
            </a>

            <a
              href="https://www.instagram.com/arontoth0/"
              target="_blank"
              rel="noopener noreferrer"
              aria-label="instagram"
            >
              <FontAwesomeIcon
                icon={["fab", "instagram"]}
                style={{ fontSize: 26 }}
              />
            </a>
          </section>
        </section>
      </section>
    </section>
  );
};

const Home = () => {
  return <span>Home</span>;
};

const About = () => {
  return <span>About</span>;
};

const Projects = () => {
  return <span>Projects</span>;
};

const Contact = () => {
  return <span>Contact</span>;
};

//make this component available to the app
export default Hamburger;
