//import liraries
import React, { createRef, useState } from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Gallery.scss";

library.add(fas);

// create a component
const Gallery = ({ setPage, page, style, children, src }) => {
  const [full, setFull] = useState(0);
  let slider = createRef();

  let refs = [];

  let images = src.map((item, index) => {
    refs[index] = createRef();
    return (
      <div key={"images" + Math.random(index)}>
        <img
          src={item}
          ref={refs[index]}
          alt="loaded source"
          onClick={(e) => !full && toggleFullScreen(e)}
        />
      </div>
    );
  });

  let pageNumber = images.length;

  const handlePrevPic = (e) => {
    if (e.target === e.currentTarget) {
      page > 0 && setPage(page - 1);
      scrollPrev();
    }
  };
  const handleNextPic = (e) => {
    if (e.target === e.currentTarget) {
      page < pageNumber - 1 && setPage(page + 1);
      scrollNext();
    }
  };

  const scrollNext = () => {
    if (page < pageNumber + 1) {
      const imgWidth = refs[page].current.offsetWidth;
      slider.current.scrollLeft += imgWidth;
    }
  };

  const scrollPrev = () => {
    if (page > 0) {
      const imgWidth = refs[page].current.offsetWidth;
      slider.current.scrollLeft -= imgWidth;
    }
  };

  const toggleFullScreen = (e) => {
    e.preventDefault();
    console.log("------ Full screen ------");
    console.log(e.target);
    console.log("------ Full screen currentTarget ------");
    console.log(e.currentTarget);
    if (e.target === e.currentTarget) {
      setFull(!full);
    }
  };

  return (
    <article
      className={`gallery-view${full ? " fullscreen" : ""}`}
      onClick={(e) => full && toggleFullScreen(e)}
    >
      <section className="gallery" style={style ? style : {}}>
        {src.length > 2 && (
          <section className="gallery-navigation">
            <button
              className="left-gallery-button"
              aria-label="previous"
              onClick={handlePrevPic}
            >
              <i
                className="fas fa-chevron-circle-left"
                onClick={handlePrevPic}
              ></i>
            </button>
            <button
              className="right-gallery-button"
              aria-label="next"
              onClick={handleNextPic}
            >
              <i
                className="fas fa-chevron-circle-right"
                onClick={handleNextPic}
              ></i>
            </button>
          </section>
        )}
        {src.length > 1 && (
          <PageIndicator selected={page} pageNumber={pageNumber} />
        )}
        <section className="gallery-images" ref={slider}>
          {images}
        </section>
      </section>
    </article>
  );
};

const PageIndicator = ({ selected, pageNumber }) => {
  let buttons = [];
  for (let i = 0; i < pageNumber; i++) {
    buttons.push(
      <button key={"buttons" + i} aria-label={`${i}pic`}>
        <FontAwesomeIcon
          icon={["fas", "circle"]}
          style={selected === i && { color: "#fff" }}
        />
      </button>
    );
  }
  return <section className="gallery-page-indicator">{buttons}</section>;
};

//make this component available to the app
export default Gallery;
