//import liraries
import React, { useState } from "react";
import { motion } from "framer-motion";
import { useHistory } from "react-router-dom";
import { PageSwing } from "../Three/PageSwing";

import "./Headline.scss";

// create a component
const Headline = (props) => {
  const [swing, setSwing] = useState(false);
  const history = useHistory();
  const { read, top, view } = props;

  const stopSwing = (page) => {
    setSwing(false);

    if (page !== "/blog") {
      history.push(page);
    } else {
      window.open("https://blog.artonwork.com", "_blank", "noopener");
    }
  };

  const handleSwing = (page) => {
    setSwing(true);
    if (!view) {
      setTimeout(() => stopSwing(page), 1500);
    } else {
      stopSwing(page);
    }
  };

  const animate = {
    active: {
      y: "0",
      opacity: "100%",
    },
    inActive: {
      y: "-400%",
      opacity: "30%",
    },
  };

  return (
    <motion.div
      id="headline"
      animate={!read ? "active" : "inActive"}
      variants={animate}
      transition={{
        duration: 0.15,
      }}
      style={{
        backgroundColor: top ? "#15022beb" : "transparent",
      }}
    >
      <section className="headline-helper">
        <div onClick={() => handleSwing("/")}>
          <h4>home</h4>
        </div>
        <div onClick={() => handleSwing("/projects")}>
          <h4>projects</h4>
        </div>
        <div onClick={() => handleSwing("/blog")}>
          <h4>blog</h4>
        </div>
      </section>
      {!view && swing && <PageSwing />}
    </motion.div>
  );
};

//make this component available to the app
export default Headline;
