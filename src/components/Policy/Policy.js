//import liraries
import React, { useState } from "react";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useCookies } from "react-cookie";

import "./Policy.scss";

// create a component
const Policy = (props) => {
  const [cookie, setCookie] = useCookies("name");
  const [hide, setHide] = useState(true);

  const week = new Date();
  week.setDate(week.getDate() + 7);

  0 && console.log(cookie);

  return (
    hide && (
      <section className="policy-container">
        <div id="policy">
          <div id="policy-content">
            <span>By using this site you agree with our </span>
            <a
              id="policy-close"
              href="https://www.privacypolicies.com/privacy/view/6b5b77f18c4b09bbe6cf57d0a4845c24"
              target="_blank"
              rel="noopener noreferrer"
            >
              Privacy Policy
            </a>
          </div>
          <div
            id="policy-close"
            onClick={() => {
              setCookie("artonwork", "policy", { path: "/", expires: week });
              setHide(false);
            }}
          >
            <FontAwesomeIcon icon={faTimes} style={{ fontSize: 13 }} />
          </div>
        </div>
      </section>
    )
  );
};

//make this component available to the app
export default Policy;
