//import liraries
import React from "react";

import "./About.scss";

// create a component
const About = () => {
  return (
    <div id="about">
      <h2>This is my portfolio site.</h2>
      <span className="qusetion">Written in:</span>
      <span className="answer">React</span>
      <span className="qusetion">Packages used:</span>
      <span className="answer">Redux, Framer-motion, Fortawesome</span>
      <span className="qusetion">Image license:</span>
      <span className="answer">Unknown (Background Image)</span>
    </div>
  );
};

//make this component available to the app
export default About;
