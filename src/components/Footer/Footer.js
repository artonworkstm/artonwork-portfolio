//import liraries
import React from "react";
import { motion } from "framer-motion";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Footer.scss";

library.add(fab, fas);

const animate = {
  active: {
    y: "0vh",
    x: "0%",
    rotate: 0,
  },
  inactive: {
    y: "-20vh",
    x: "-60%",
    rotate: "90deg",
  },
};

const iconAnimate = {
  active: {
    rotate: 0,
  },
  inactive: {
    rotate: "-90deg",
  },
};

// create a component
const Footer = (props) => {
  const { read } = props;
  return (
    <motion.div
      id="footer"
      animate={read ? "inactive" : "active"}
      variants={animate}
    >
      <motion.div animate={read ? "inactive" : "active"} variants={iconAnimate}>
        <a
          href="https://github.com/Puding07"
          target="_blank"
          rel="noopener noreferrer"
          aria-label="github"
        >
          <FontAwesomeIcon icon={["fab", "github"]} style={{ fontSize: 26 }} />
        </a>
      </motion.div>
      <motion.div animate={read ? "inactive" : "active"} variants={iconAnimate}>
        <a
          href="https://www.linkedin.com/in/%C3%A1ron-t%C3%B3th-5825841a8/"
          target="_blank"
          rel="noopener noreferrer"
          aria-label="linkedin"
        >
          <FontAwesomeIcon
            icon={["fab", "linkedin-in"]}
            style={{ fontSize: 26 }}
          />
        </a>
      </motion.div>
      <motion.div animate={read ? "inactive" : "active"} variants={iconAnimate}>
        <a
          href="https://www.instagram.com/arontoth0/"
          target="_blank"
          rel="noopener noreferrer"
          aria-label="instagram"
        >
          <FontAwesomeIcon
            icon={["fab", "instagram"]}
            style={{ fontSize: 26 }}
          />
        </a>
      </motion.div>
    </motion.div>
  );
};

//make this component available to the app
export default Footer;
