//import liraries
import React from "react";

import "./Contact.scss";

// create a component
const Contact = () => {

  return (
    <div id="contact">
      <h2>Aaron Tóth</h2>
      <h2>bs.aaron.toth@gmail.com</h2>
    </div>
  );
};

//make this component available to the app
export default Contact;
