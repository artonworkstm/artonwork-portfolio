import React, { useState } from "react";
import Gallery from "../Gallery/Gallery";
import { imgPath } from "../../config.js";
import { jp2Detect, jxrDetect, webpDetect } from "../../bowserDetect";
import "./Projects.scss";
import ProjectsLaying from "../Three/ProjectsLaying";

const Projects = () => {
  const [vibePage, setVibePage] = useState(0);
  const [phPage, setPhPage] = useState(0);
  const [pomoPage, setPomoPage] = useState(0);

  /**
   * Detect browser and import the browser specific image for performance optim.
   */
  let phLogin,
    phMain,
    phPwReset,
    pomodoroDesktop,
    vibeApp,
    vibeFinal,
    vibeBox,
    vibePcbDown,
    vibePcbTop;

  if (webpDetect()) {
    phLogin = imgPath("ph-login", "webp");
    phMain = imgPath("ph-main", "webp");
    phPwReset = imgPath("ph-pw-reset", "webp");
    pomodoroDesktop = imgPath("pomodoro-desktop", "webp");
    vibeApp = imgPath("app", "webp");
    vibeFinal = imgPath("final", "webp");
    vibeBox = imgPath("box", "webp");
    vibePcbDown = imgPath("pcb_down", "webp");
    vibePcbTop = imgPath("pcb_top", "webp");
  } else if (jp2Detect()) {
    phLogin = imgPath("ph-login", "jp2");
    phMain = imgPath("ph-main", "jp2");
    phPwReset = imgPath("ph-pw-reset", "jp2");
    pomodoroDesktop = imgPath("pomodoro-desktop", "jp2");
  } else if (jxrDetect()) {
    phLogin = imgPath("ph-login", "jxr");
    phMain = imgPath("ph-main", "jxr");
    phPwReset = imgPath("ph-pw-reset", "jxr");
    pomodoroDesktop = imgPath("pomodoro-desktop", "jxr");
  } else {
    phLogin = imgPath("ph-login", "png");
    phMain = imgPath("ph-main", "png");
    phPwReset = imgPath("ph-pw-reset", "png");
    pomodoroDesktop = imgPath("pomodoro-desktop", "png");
    vibeApp = imgPath("app", "png");
    vibeFinal = imgPath("final", "png");
    vibeBox = imgPath("box", "png");
    vibePcbDown = imgPath("pcb_down", "png");
    vibePcbTop = imgPath("pcb_top", "png");
  }

  return (
    <div className="projects">
      <ProjectsLaying />
      <section className="project vibe">
        <section className="gallerys">
          <Gallery
            setPage={setVibePage}
            page={vibePage}
            src={[vibeFinal, vibeApp, vibeBox, vibePcbDown, vibePcbTop]}
          />
        </section>
        <section className="texts">
          <h2>
            <a
              href="https://vibe-light.artonwork.com/"
              target="__blank"
              rel="noopener noreferrer"
              aria-label="Vibe-Light"
            >
              Vibe Light
            </a>
          </h2>
          <section className="parag">
            <span>
              This is an IoT app for controlling an RGB light. Because the app
              is PWA (it only runs trough https) the microcontroller runs a TLS
              Web server too. The controller is an ESP8266 so there is a 1-3
              seconds delay for now, but I'm working on to improve it by caching
              the handshakes, which will reduce the response time a lot. I
              designed the schematics too.
            </span>
          </section>
          <section className="parag">
            <span>
              I have to say that everything is in prototype status excluding the
              PWA. Once I have the CNC machine built, the PCB will be half the
              size of the current one. Also it uses active cooling because it
              turned out that 3W (50%) dissipation is warm :D.
            </span>
          </section>
        </section>
      </section>
      <section className="project ph">
        <section className="gallerys">
          <Gallery
            setPage={setPhPage}
            page={phPage}
            src={[phLogin, phMain, phPwReset]}
          />
        </section>
        <section className="texts">
          <h2>Ph Controller</h2>
          <section className="parag">
            <span>
              This is an IoT app for controlling the Ph value of the water. The
              app uses Firrebase and Firrebase's realtime database.
            </span>
          </section>
          <section className="parag">
            <span>
              On the other half a Wemos D1 mini pro connects to Firebase,
              controlls the water pumps and consumes value from the ph-probe.
            </span>
          </section>
        </section>
      </section>
      <section className="project pomodoro">
        <section className="gallerys">
          <Gallery
            setPage={setPomoPage}
            page={pomoPage}
            src={[pomodoroDesktop]}
          />
        </section>
        <section className="texts">
          <h2>
            <a
              href="https://pomodoro.artonwork.com/"
              target="__blank"
              rel="noopener noreferrer"
              aria-label="Pomodoro-Timer"
            >
              Pomodoro Timer PWA
            </a>
          </h2>
          <section className="parag">
            <span>
              Pomodoro Timer is a Progressive Web Application PWA for short.
              PWA's benefit is they are lightweight, work offline too and can be
              installed to any device with just a click.
            </span>
          </section>
          <section className="parag">
            <span>
              PWAs uses the web browser that means they are secure, they can't
              change your device data. They can only use microphone, gps, camera
              if you give permission to them.
            </span>
          </section>
        </section>
      </section>
    </div>
  );
};

export default Projects;
