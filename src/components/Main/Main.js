//import liraries
import React from "react";
import { Logo } from "./Logo";
import Animations from "../Three/Animations";

import "./Main.scss";
import Waving from "../Three/Waving";

// create a component
const Main = ({ view }) => {
  return (
    <article className="main">
      <section className="artonwork">
        <Logo />
        {!view && <Animations />}
        <section className="description">
          <section className="parag">
            <span>
              Artonwork is my personal website made to showcase my projects. My
              main goal is set by perfectionism, to create useful, fast and
              reliable products.
            </span>
          </section>
          <section className="parag">
            <span>
              I build Native and Progressive Web appliactions. I try my best at
              desiging to create beautiful user interfaces with stunning user
              experiences.
            </span>
          </section>
        </section>
      </section>

      <section className="intro">
        <section className="stat">
          <Waving />
          <h2>Aaron Tóth</h2>
          <span className="question">Language:</span>
          <span className="answers">JS/TS (ES8), CSS, SQL, C/C++</span>
        </section>

        <section className="description">
          <section>
            <h1>About me</h1>

            <div className="parag">
              <span>
                I'm maker, and my passion is software development. It has always
                been an adventure creating projects and getting trough
                challenges set by bugs and difficult tasks.
              </span>
            </div>
            <div className="parag">
              <span>
                I rather build than buy the tools I need. I just love DIY
                projects, becuase I always learn something new with them and I
                can customize anything without voiding any warranty.
              </span>
            </div>
            <div className="parag">
              <span>
                Now I design PCBs to breadboard but my main project is building
                a Laser engraver so I can etch and make my own 2 layered PCBs.
              </span>
            </div>
            <div className="parag">
              <span>
                If you think I may be the perfect choice as a developer feel
                free to connect me!
              </span>
            </div>
          </section>
        </section>
      </section>
    </article>
  );
};

//make this component available to the app
export default Main;
