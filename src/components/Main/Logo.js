//import liraries
import React from "react";
import { motion } from "framer-motion";

export const Logo = () => {
  const icon = {
    hidden: {
      opacity: 1,
      strokeDasharray: "0px",
      pathLength: 0,
      fill: "rgba(255, 255, 255, 0)",
    },
    visible: {
      opacity: 1,
      strokeDasharray: "100px",
      pathLength: 1,
      fill: "rgba(255, 255, 255, 1)",
    },
  };
  return (
    <svg
      id="logo"
      viewBox="0 0 478 126"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <motion.path
        d="M68.988 66V14.4C68.988 11.5333 69.9913 9.09667 71.998 7.09C74.0047 5.08333 76.4413 4.08 79.308 4.08H120.588C123.455 4.08 125.891 5.08333 127.898 7.09C129.905 9.09667 130.908 11.5333 130.908 14.4V66H123.942V43.382H75.954V66H68.988ZM75.954 36.416H123.942V14.4C123.942 13.4827 123.598 12.7087 122.91 12.078C122.279 11.39 121.505 11.046 120.588 11.046H79.308C78.3907 11.046 77.588 11.39 76.9 12.078C76.2693 12.7087 75.954 13.4827 75.954 14.4V36.416Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M3.76778 9.23224L116.268 121.732L112.732 125.268L0.232239 12.7678L3.76778 9.23224Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M145 65.88V26.406C145 23.5393 146.003 21.1027 148.01 19.096C150.074 17.032 152.539 16 155.406 16H183.442V23.052H155.406C154.489 23.052 153.686 23.396 152.998 24.084C152.367 24.7147 152.052 25.4887 152.052 26.406V65.88H145Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M202.406 66.22C199.482 66.22 197.017 65.2167 195.01 63.21C193.003 61.146 192 58.6807 192 55.814V0H199.052V16.34H220.638V23.392H199.052V55.814C199.052 56.7313 199.367 57.534 199.998 58.222C200.686 58.8527 201.489 59.168 202.406 59.168H220.638V66.22H202.406Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M244.792 66C241.925 66 239.46 64.9967 237.396 62.99C235.389 60.926 234.386 58.4607 234.386 55.594V26.526C234.386 23.6593 235.389 21.2227 237.396 19.216C239.46 17.152 241.925 16.12 244.792 16.12H274.72C277.644 16.12 280.109 17.152 282.116 19.216C284.18 21.2227 285.212 23.6593 285.212 26.526V55.594C285.212 58.4607 284.18 60.926 282.116 62.99C280.109 64.9967 277.644 66 274.72 66H244.792ZM244.792 58.948H274.72C275.637 58.948 276.411 58.6327 277.042 58.002C277.73 57.314 278.074 56.5113 278.074 55.594V26.526C278.074 25.6087 277.73 24.8347 277.042 24.204C276.411 23.516 275.637 23.172 274.72 23.172H244.792C243.875 23.172 243.072 23.516 242.384 24.204C241.753 24.8347 241.438 25.6087 241.438 26.526V55.594C241.438 56.5113 241.753 57.314 242.384 58.002C243.072 58.6327 243.875 58.948 244.792 58.948Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M300.644 66V16.12H341.064C343.931 16.12 346.367 17.152 348.374 19.216C350.438 21.2227 351.47 23.6593 351.47 26.526V66H344.418V26.526C344.418 25.6087 344.074 24.8347 343.386 24.204C342.755 23.516 341.981 23.172 341.064 23.172H311.136C310.219 23.172 309.416 23.516 308.728 24.204C308.04 24.8347 307.696 25.6087 307.696 26.526V66H300.644Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M364.768 6.23224L477.268 118.732L473.732 122.268L361.232 9.76777L364.768 6.23224Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M174.618 123L155.01 73.12H162.578L177.714 110.272L193.796 73.12H203.084L220.198 110.186L234.302 73.12H241.956L223.122 123H217.016L198.44 81.806L180.81 123H174.618Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M261.792 120C258.925 120 256.46 118.997 254.396 116.99C252.389 114.926 251.386 112.461 251.386 109.594V80.526C251.386 77.6593 252.389 75.2227 254.396 73.216C256.46 71.152 258.925 70.12 261.792 70.12H291.72C294.644 70.12 297.109 71.152 299.116 73.216C301.18 75.2227 302.212 77.6593 302.212 80.526V109.594C302.212 112.461 301.18 114.926 299.116 116.99C297.109 118.997 294.644 120 291.72 120H261.792ZM261.792 112.948H291.72C292.637 112.948 293.411 112.633 294.042 112.002C294.73 111.314 295.074 110.511 295.074 109.594V80.526C295.074 79.6087 294.73 78.8347 294.042 78.204C293.411 77.516 292.637 77.172 291.72 77.172H261.792C260.875 77.172 260.072 77.516 259.384 78.204C258.753 78.8347 258.438 79.6087 258.438 80.526V109.594C258.438 110.511 258.753 111.314 259.384 112.002C260.072 112.633 260.875 112.948 261.792 112.948Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M313.472 120V80.526C313.472 77.6593 314.475 75.2227 316.482 73.216C318.546 71.152 321.011 70.12 323.878 70.12H351.914V77.172H323.878C322.961 77.172 322.158 77.516 321.47 78.204C320.839 78.8347 320.524 79.6087 320.524 80.526V120H313.472Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
      <motion.path
        d="M360.644 120V53.78H367.696V91.534H381.456L401.064 70.12H410.352V70.206L387.734 95.06L410.266 119.914V120H401.064L381.456 98.586H367.696V120H360.644Z"
        fill="black"
        stroke={"#fff"}
        strokeWidth={"1"}
        variants={icon}
        initial="hidden"
        animate="visible"
        transition={{
          default: { duration: 2, ease: "easeInOut" },
          fill: { duration: 2, ease: [1, 0, 0.8, 1] },
        }}
      />
    </svg>
  );
};
