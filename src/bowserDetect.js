export const webpDetect = () => {
    // Opera 8.0+
    const isOpera =
    (!!window.opr && !!window.opr.addons) ||
    !!window.opera ||
    navigator.userAgent.indexOf(" OPR/") >= 0;

    // Firefox 1.0+
    const isFirefox = typeof InstallTrigger !== "undefined";

    // Edge 20+
    const isEdge = !jxrDetect() && !!window.StyleMedia;

    // Chrome 1 - 79
    const isChrome =
    !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

    // Edge (based on chromium) detection
    const isEdgeChromium = isChrome && navigator.userAgent.indexOf("Edg") !== -1;

    if(isEdge || isFirefox || isChrome || isOpera || isEdgeChromium) {
        return true;
    }

    return false;
}

export const jp2Detect = () => {
    // Safari 3.0+ "[object HTMLElementConstructor]"
    const isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window["safari"] || typeof safari !== "undefined");

    if(isSafari) {
        return true;
    }

    return false;
}

export const jxrDetect = () => {
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/ false || !!document.documentMode;

    if(isIE) {
        return true;
    }

    return false;
}